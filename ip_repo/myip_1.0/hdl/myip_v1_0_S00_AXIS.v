
`timescale 1 ns / 1 ps

	module myip_v1_0_S00_AXIS #
	(
		// Users to add parameters here
	  // User parameters ends
      // Do not modify the parameters beyond this line
       parameter integer CONFIG_WIDTH = 32,
       parameter integer FEATURE_WIDTH = 8,
       parameter integer FEATURE_ROW = 6,
       parameter integer FEATURE_COL = 12,
       parameter integer WEIGHT_SIZE_WIDTH = 64, // 7x9, the last bit is redundant
       parameter integer WEIGHT_UINDEX = 4,
       parameter integer TENSOR_SLICE = 8, // 4 per slice, 8 slices in total
       parameter integer BIAS_WIDTH = 8,
       parameter integer TENSOR_WIDTH = 8,
      // AXI4Stream sink: Data Width
      parameter integer C_S_AXIS_TDATA_WIDTH    = 32
  )
  (
      // Users to add ports here
  
      // User ports ends
      // Do not modify the ports beyond this line
  
      // AXI4Stream sink: Clock
      input wire  S_AXIS_ACLK,
      // AXI4Stream sink: Reset
      input wire  S_AXIS_ARESETN,
      // Ready to accept data in
      output wire  S_AXIS_TREADY,
      // Data in
      input wire [C_S_AXIS_TDATA_WIDTH-1 : 0] S_AXIS_TDATA,
      // Byte qualifier
      input wire [(C_S_AXIS_TDATA_WIDTH/8)-1 : 0] S_AXIS_TSTRB,
      // Indicates boundary of last packet
      input wire  S_AXIS_TLAST,
      // Data is in valid
      input wire  S_AXIS_TVALID,
      
      // output tensor to axi
        output wire [63:0] tensorOut
  );
  // function called clogb2 that returns an integer which has the 
  // value of the ceiling of the log base 2.
  function integer clogb2 (input integer bit_depth);
    begin
      for(clogb2=0; bit_depth>0; clogb2=clogb2+1)
        bit_depth = bit_depth >> 1;
    end
  endfunction
  
  // Total number of input data.
  localparam NUMBER_OF_INPUT_WORDS  = 128;
  // bit_num gives the minimum number of bits needed to address 'NUMBER_OF_INPUT_WORDS' size of FIFO.
  localparam bit_num  = clogb2(NUMBER_OF_INPUT_WORDS-1);
  // Define the states of state machine
  // The control state machine oversees the writing of input streaming data to the FIFO,
  // and outputs the streaming data from the FIFO
  parameter [1:0] IDLE = 1'b0,        // This is the initial/idle state 
  
                  WRITE_FIFO  = 1'b1; // In this state FIFO is written with the
                                      // input stream data S_AXIS_TDATA 
  wire      axis_tready;
  // State variable
  reg mst_exec_state;  
  // FIFO implementation signals
  genvar byte_index;
  genvar i;     
  // FIFO write enable
  wire fifo_wren;
  // FIFO full flag
  reg fifo_full_flag;
  // FIFO write pointer
  reg [bit_num-1:0] write_pointer;
  // sink has accepted all the streaming data and stored in FIFO
  reg writes_done;
  // I/O Connections assignments
  
  assign S_AXIS_TREADY    = axis_tready;
  // Control state machine implementation
  always @(posedge S_AXIS_ACLK) 
  begin  
    if (!S_AXIS_ARESETN) 
    // Synchronous reset (active low)
      begin
        mst_exec_state <= IDLE;
      end  
    else
      case (mst_exec_state)
        IDLE: 
          // The sink starts accepting tdata when 
          // there tvalid is asserted to mark the
          // presence of valid streaming data 
            if (S_AXIS_TVALID)
              begin
                mst_exec_state <= WRITE_FIFO;
              end
            else
              begin
                mst_exec_state <= IDLE;
              end
        WRITE_FIFO: 
          // When the sink has accepted all the streaming input data,
          // the interface swiches functionality to a streaming master
          if (writes_done)
            begin
              mst_exec_state <= IDLE;
            end
          else
            begin
              // The sink accepts and stores tdata 
              // into FIFO
              mst_exec_state <= WRITE_FIFO;
            end
  
      endcase
  end
  // AXI Streaming Sink 
  // 
  // The example design sink is always ready to accept the S_AXIS_TDATA  until
  // the FIFO is not filled with NUMBER_OF_INPUT_WORDS number of input words.
  assign axis_tready = ((mst_exec_state == WRITE_FIFO) && (write_pointer <= NUMBER_OF_INPUT_WORDS-1));
  
  always@(posedge S_AXIS_ACLK)
  begin
    if(!S_AXIS_ARESETN)
      begin
        write_pointer <= 0;
        writes_done <= 1'b0;
      end  
    else
      if (write_pointer <= NUMBER_OF_INPUT_WORDS-1)
        begin
          if (fifo_wren)
            begin
              // write pointer is incremented after every write to the FIFO
              // when FIFO write signal is enabled.
              write_pointer <= write_pointer + 1;
              writes_done <= 1'b0;
            end
            if ((write_pointer == NUMBER_OF_INPUT_WORDS-1)|| S_AXIS_TLAST)
              begin
                // reads_done is asserted when NUMBER_OF_INPUT_WORDS numbers of streaming data 
                // has been written to the FIFO which is also marked by S_AXIS_TLAST(kept for optional usage).
                writes_done <= 1'b1;
              end
        end  
  end
  
  // FIFO write enable generation
  assign fifo_wren = S_AXIS_TVALID && axis_tready;
  reg [31:0] fifo [NUMBER_OF_INPUT_WORDS - 1:0];
  // FIFO Implementation
  generate 
    for(byte_index=0; byte_index<= (C_S_AXIS_TDATA_WIDTH/8-1); byte_index=byte_index+1)
    begin:FIFO_GEN
  
      // reg  [(C_S_AXIS_TDATA_WIDTH/4)-1:0] stream_data_fifo [0 : NUMBER_OF_INPUT_WORDS-1];     
      
      // Streaming input data is stored in FIFO
      
      always @( posedge S_AXIS_ACLK )
      begin
        if (fifo_wren)// && S_AXIS_TSTRB[byte_index])
          begin
            fifo[write_pointer][(byte_index*8+7) -: 8] <= S_AXIS_TDATA[(byte_index*8+7) -: 8];
          end  
      end  
    end        
  endgenerate
     wire    [CONFIG_WIDTH-1 : 0]                         cfg_reg;
     wire    [FEATURE_WIDTH*FEATURE_ROW*FEATURE_COL-1: 0] feature_buddle ;
     wire    [WEIGHT_SIZE_WIDTH*WEIGHT_UINDEX-1 : 0]      weight_bundles[7:0];
     wire    [BIAS_WIDTH*TENSOR_SLICE-1 : 0]              bias_buddle;
     wire    [TENSOR_WIDTH*TENSOR_SLICE-1 : 0]            tensor_output_buddle;
     wire    [127:0] irst; 
     assign cfg_reg = fifo[0];
     assign feature_buddle = {fifo[1], fifo[2], fifo[3], fifo[4], fifo[5], fifo[6], 
                              fifo[7], fifo[8], fifo[9], fifo[10], fifo[11], fifo[12],
                              fifo[13], fifo[14], fifo[15], fifo[16], fifo[17], fifo[18]};
     for (i = 0; i < 8; i=i+1)
     begin
         assign weight_bundles[i] = {fifo[i * 8 + 19], fifo[i * 8 + 20], fifo[i * 8 + 21]
                                          , fifo[i * 8 + 22], fifo[i * 8 + 23], fifo[i * 8 + 24]
                                          , fifo[i * 8 + 25], fifo[i * 8 + 26]};
     end
  // Add user logic here
  assign tensorOut = tensor_output_buddle;
  
  
  tensor_core tc1(
      .clk(S_AXIS_ACLK),
      .rst(S_AXIS_ARESETN),
      // input
      .cfg_reg(cfg_reg),
      .feature(feature_buddle),
      .weight1(weight_bundles[0]),
      .weight2(weight_bundles[1]),
      .weight3(weight_bundles[2]),
      .weight4(weight_bundles[3]),
      .weight5(weight_bundles[4]),
      .weight6(weight_bundles[5]),
      .weight7(weight_bundles[6]),
      .weight8(weight_bundles[7]), 
      .bias8(bias_buddle), 
      // output
      .tnsout(tensor_output_buddle),
      .irst(irst)   
      );
  // User logic ends
  
  endmodule
  
  module tensor_core #
  (
  // Accelerator based infomation
          parameter integer CONFIG_WIDTH = 32,
          parameter integer FEATURE_WIDTH = 8,
          parameter integer FEATURE_ROW = 6,
          parameter integer FEATURE_COL = 12,
          parameter integer WEIGHT_SIZE_WIDTH = 64, // 7x9, the last bit is redundant
          parameter integer WEIGHT_UINDEX = 4,
          parameter integer TENSOR_SLICE = 8, // 4 per slice, 8 slices in total
          parameter integer BIAS_WIDTH = 8,
          parameter integer TENSOR_WIDTH = 8
  )
  (
  input    clk, 
  input    rst,
  input    [CONFIG_WIDTH-1 : 0]                         cfg_reg,
  input    [FEATURE_WIDTH*FEATURE_ROW*FEATURE_COL-1: 0] feature ,
  input    [WEIGHT_SIZE_WIDTH*WEIGHT_UINDEX-1 : 0]      weight1 ,
  input    [WEIGHT_SIZE_WIDTH*WEIGHT_UINDEX-1 : 0]      weight2 ,
  input    [WEIGHT_SIZE_WIDTH*WEIGHT_UINDEX-1 : 0]      weight3 ,
  input    [WEIGHT_SIZE_WIDTH*WEIGHT_UINDEX-1 : 0]      weight4 ,
  input    [WEIGHT_SIZE_WIDTH*WEIGHT_UINDEX-1 : 0]      weight5 ,
  input    [WEIGHT_SIZE_WIDTH*WEIGHT_UINDEX-1 : 0]      weight6 ,
  input    [WEIGHT_SIZE_WIDTH*WEIGHT_UINDEX-1 : 0]      weight7 ,
  input    [WEIGHT_SIZE_WIDTH*WEIGHT_UINDEX-1 : 0]      weight8 ,
  input    [BIAS_WIDTH*TENSOR_SLICE-1 : 0]              bias8,
  output   [TENSOR_WIDTH*TENSOR_SLICE-1 : 0]            tnsout,
  output   [127:0] irst
  );
  
  
  // cfg_reg format
  //  | 31 -- 28 | 27 -- 26 | 25 -- 0 |
  //  | FS,X-4b  | FS,Y-2b  | Send to slice|
  
  // Feature Selection , like sliding
  
  reg [FEATURE_WIDTH*FEATURE_COL-1: 0] col_selection1, col_selection2,col_selection3;
  reg [FEATURE_WIDTH*9-1: 0]           wnd_3x3;
  
  always @(*) begin
      case (cfg_reg[27:26])
          2'b00: begin
              col_selection1 = feature[FEATURE_WIDTH*FEATURE_COL-1:0];
              col_selection2 = feature[FEATURE_WIDTH*FEATURE_COL*2-1:FEATURE_WIDTH*FEATURE_COL];
              col_selection3 = feature[FEATURE_WIDTH*FEATURE_COL*3-1:FEATURE_WIDTH*FEATURE_COL*2];
          end
          2'b01: begin
              col_selection1 = feature[FEATURE_WIDTH*FEATURE_COL*2-1:FEATURE_WIDTH*FEATURE_COL];
              col_selection2 = feature[FEATURE_WIDTH*FEATURE_COL*3-1:FEATURE_WIDTH*FEATURE_COL*2];
              col_selection3 = feature[FEATURE_WIDTH*FEATURE_COL*4-1:FEATURE_WIDTH*FEATURE_COL*3];
          end
          2'b10: begin
              col_selection1 = feature[FEATURE_WIDTH*FEATURE_COL*3-1:FEATURE_WIDTH*FEATURE_COL*2];
              col_selection2 = feature[FEATURE_WIDTH*FEATURE_COL*4-1:FEATURE_WIDTH*FEATURE_COL*3];
              col_selection3 = feature[FEATURE_WIDTH*FEATURE_COL*5-1:FEATURE_WIDTH*FEATURE_COL*4];
          end
          2'b11: begin
              col_selection1 = feature[FEATURE_WIDTH*FEATURE_COL*4-1:FEATURE_WIDTH*FEATURE_COL*3];
              col_selection2 = feature[FEATURE_WIDTH*FEATURE_COL*5-1:FEATURE_WIDTH*FEATURE_COL*4];
              col_selection3 = feature[FEATURE_WIDTH*FEATURE_COL*6-1:FEATURE_WIDTH*FEATURE_COL*5];
          end
      endcase
  end
  
  always @(*) begin
      case (cfg_reg[31:28])
          4'd0: wnd_3x3= {col_selection3[FEATURE_WIDTH*FEATURE_COL-1:FEATURE_WIDTH*FEATURE_COL-24], 
              col_selection2[FEATURE_WIDTH*FEATURE_COL-1:FEATURE_WIDTH*FEATURE_COL-24], 
              col_selection1[FEATURE_WIDTH*FEATURE_COL-1:FEATURE_WIDTH*FEATURE_COL-24]};
          
          4'd1: wnd_3x3= {col_selection3[FEATURE_WIDTH*FEATURE_COL-9:FEATURE_WIDTH*FEATURE_COL-32], 
              col_selection2[FEATURE_WIDTH*FEATURE_COL-9:FEATURE_WIDTH*FEATURE_COL-32], 
              col_selection1[FEATURE_WIDTH*FEATURE_COL-9:FEATURE_WIDTH*FEATURE_COL-32]};
          
          4'd2: wnd_3x3= {col_selection3[FEATURE_WIDTH*FEATURE_COL-17:FEATURE_WIDTH*FEATURE_COL-40], 
              col_selection2[FEATURE_WIDTH*FEATURE_COL-17:FEATURE_WIDTH*FEATURE_COL-40], 
              col_selection1[FEATURE_WIDTH*FEATURE_COL-17:FEATURE_WIDTH*FEATURE_COL-40]};
          
          4'd3: wnd_3x3= {col_selection3[FEATURE_WIDTH*FEATURE_COL-25:FEATURE_WIDTH*FEATURE_COL-48], 
              col_selection2[FEATURE_WIDTH*FEATURE_COL-25:FEATURE_WIDTH*FEATURE_COL-48], 
              col_selection1[FEATURE_WIDTH*FEATURE_COL-25:FEATURE_WIDTH*FEATURE_COL-48]};
  
          4'd4: wnd_3x3= {col_selection3[FEATURE_WIDTH*FEATURE_COL-33:FEATURE_WIDTH*FEATURE_COL-56], 
              col_selection2[FEATURE_WIDTH*FEATURE_COL-33:FEATURE_WIDTH*FEATURE_COL-56], 
              col_selection1[FEATURE_WIDTH*FEATURE_COL-33:FEATURE_WIDTH*FEATURE_COL-56]};
          
          4'd5: wnd_3x3= {col_selection3[FEATURE_WIDTH*FEATURE_COL-41:FEATURE_WIDTH*FEATURE_COL-64], 
              col_selection2[FEATURE_WIDTH*FEATURE_COL-41:FEATURE_WIDTH*FEATURE_COL-64], 
              col_selection1[FEATURE_WIDTH*FEATURE_COL-41:FEATURE_WIDTH*FEATURE_COL-64]};
  
          4'd6: wnd_3x3= {col_selection3[FEATURE_WIDTH*FEATURE_COL-49:FEATURE_WIDTH*FEATURE_COL-72], 
              col_selection2[FEATURE_WIDTH*FEATURE_COL-49:FEATURE_WIDTH*FEATURE_COL-72], 
              col_selection1[FEATURE_WIDTH*FEATURE_COL-49:FEATURE_WIDTH*FEATURE_COL-72]};
  
          4'd7: wnd_3x3= {col_selection3[FEATURE_WIDTH*FEATURE_COL-57:FEATURE_WIDTH*FEATURE_COL-80], 
              col_selection2[FEATURE_WIDTH*FEATURE_COL-57:FEATURE_WIDTH*FEATURE_COL-80], 
              col_selection1[FEATURE_WIDTH*FEATURE_COL-57:FEATURE_WIDTH*FEATURE_COL-80]};
  
          4'd8: wnd_3x3= {col_selection3[FEATURE_WIDTH*FEATURE_COL-65:FEATURE_WIDTH*FEATURE_COL-88], 
              col_selection2[FEATURE_WIDTH*FEATURE_COL-65:FEATURE_WIDTH*FEATURE_COL-88], 
              col_selection1[FEATURE_WIDTH*FEATURE_COL-65:FEATURE_WIDTH*FEATURE_COL-88]};
  
          4'd9: wnd_3x3= {col_selection3[FEATURE_WIDTH*FEATURE_COL-73:FEATURE_WIDTH*FEATURE_COL-96], 
              col_selection2[FEATURE_WIDTH*FEATURE_COL-73:FEATURE_WIDTH*FEATURE_COL-96], 
              col_selection1[FEATURE_WIDTH*FEATURE_COL-73:FEATURE_WIDTH*FEATURE_COL-96]};
          
          default: wnd_3x3=0;
      endcase
  end
  
  // Generate 8 slices

  // for debug only
  
  tensor_slice ts1(
  .clk(clk), 
  .rst(rst),
  .xcfg_reg(cfg_reg[25:0]),
  .xwnd_3x3(wnd_3x3),
  .xweight(weight1),
  .xbias(bias8[7:0]),
  .xsout(tnsout[7:0]),
  .ximd(irst[15:0])
  );
  
  tensor_slice ts2(
  .clk(clk), 
  .rst(rst),
  .xcfg_reg(cfg_reg[25:0]),
  .xwnd_3x3(wnd_3x3),
  .xweight(weight2),
  .xbias(bias8[15:8]),
  .xsout(tnsout[15:8]),
  .ximd(irst[31:16])
  );
  
  tensor_slice ts3(
  .clk(clk), 
  .rst(rst),
  .xcfg_reg(cfg_reg[25:0]),
  .xwnd_3x3(wnd_3x3),
  .xweight(weight3),
  .xbias(bias8[23:16]),
  .xsout(tnsout[23:16]),
  .ximd(irst[47:32])
  );
  
  tensor_slice ts4(
  .clk(clk), 
  .rst(rst),
  .xcfg_reg(cfg_reg[25:0]),
  .xwnd_3x3(wnd_3x3),
  .xweight(weight4),
  .xbias(bias8[31:24]),
  .xsout(tnsout[31:24]),
  .ximd(irst[63:48])
  );
  
  tensor_slice ts5(
  .clk(clk), 
  .rst(rst),
  .xcfg_reg(cfg_reg[25:0]),
  .xwnd_3x3(wnd_3x3),
  .xweight(weight5),
  .xbias(bias8[39:32]),
  .xsout(tnsout[39:32]),
  .ximd(irst[79:64])
  );
  
  tensor_slice ts6(
  .clk(clk), 
  .rst(rst),
  .xcfg_reg(cfg_reg[25:0]),
  .xwnd_3x3(wnd_3x3),
  .xweight(weight6),
  .xbias(bias8[47:40]),
  .xsout(tnsout[47:40]),
  .ximd(irst[95:80])
  );
  
  tensor_slice ts7(
  .clk(clk), 
  .rst(rst),
  .xcfg_reg(cfg_reg[25:0]),
  .xwnd_3x3(wnd_3x3),
  .xweight(weight7),
  .xbias(bias8[55:48]),
  .xsout(tnsout[55:48]),
  .ximd(irst[111:96])
  );
  
  tensor_slice ts8(
  .clk(clk), 
  .rst(rst),
  .xcfg_reg(cfg_reg[25:0]),
  .xwnd_3x3(wnd_3x3),
  .xweight(weight8),
  .xbias(bias8[63:56]),
  .xsout(tnsout[63:56]),
  .ximd(irst[127:112])
  );
  
  endmodule
  
  module tensor_slice #
  (
  parameter integer CONFIG_WIDTH = 26,
  parameter integer WINDOW_SIZE = 9,
  parameter integer WEIGHT_SIZE_WIDTH = 64, // 7x9, the last bit is redundant
  parameter integer WEIGHT_UINDEX = 4,
  parameter integer BIAS_WIDTH = 8,
  parameter integer TENSOR_WIDTH = 8,
  parameter integer FEATURE_WIDTH = 8
  )
  (
  input   clk, rst,
  input   [CONFIG_WIDTH-1 : 0]    xcfg_reg,
  input   [WINDOW_SIZE*FEATURE_WIDTH-1 : 0]     xwnd_3x3,
  input   [WEIGHT_SIZE_WIDTH*WEIGHT_UINDEX-1 : 0]  xweight,
  input   signed [BIAS_WIDTH-1 : 0]      xbias,
  output  signed [TENSOR_WIDTH-1 : 0]    xsout,
  output  signed [15:0] ximd
  );
  
  // xcfg_reg format
  //  | 25 --     | 24  -- 21     |20 -- 17  | 16  |  15 --12   | 2 -- 1 | 0 |
  //  | Bias/PSum | Psum Index-4b |Out Shift| Relu | Ouput Sel| Wgt Sel| Trigger|
  
  // Weight Selection
  reg [63:0] w;
  
  always@(*) begin
      case (xcfg_reg[2:1])
          2'b00: w = xweight[WEIGHT_SIZE_WIDTH-1:0];
          2'b01: w = xweight[2*WEIGHT_SIZE_WIDTH-1:WEIGHT_SIZE_WIDTH];
          2'b10: w = xweight[3*WEIGHT_SIZE_WIDTH-1:2*WEIGHT_SIZE_WIDTH];
          2'b11: w = xweight[4*WEIGHT_SIZE_WIDTH-1:3*WEIGHT_SIZE_WIDTH];
      endcase
  end
  
  // Define the scratch pad -16b
  reg signed [15:0] sp[0:15]; // or need a submodule?
  
  
  wire signed [15:0] psum;
  assign ximd = {xwnd_3x3[7:0],1'b0,w[6:0]};
  
  always @(posedge clk or negedge rst) begin
      if(~rst) begin
          sp[0] <= 0;
          sp[1] <= 0;
          sp[2] <= 0;
          sp[3] <= 0;
          sp[4] <= 0;
          sp[5] <= 0;
          sp[6] <= 0;
          sp[7] <= 0;
          sp[8] <= 0;
          sp[9] <= 0;
          sp[10] <= 0;
          sp[11] <= 0;
          sp[12] <= 0;
          sp[13] <= 0;
          sp[14] <= 0;
          sp[15] <= 0;
      end
      else begin
          sp[xcfg_reg[24:21]] <= psum;
      end
  end
  
  
  
  // Bias or partial Sum
  wire signed [15:0] adder0;
  assign adder0 = xcfg_reg[25]? (xbias <<< (xcfg_reg[20:17])):0;
  
  // 9x9 + bias/parital MAC
  wire signed [7:0] x0, x1, x2, x3, x4, x5, x6, x7, x8;
  wire signed [6:0] w0, w1, w2, w3, w4, w5, w6, w7, w8;
  
  
  assign x0 = xwnd_3x3[7:0];
  assign x1 = xwnd_3x3[15:8];
  assign x2 = xwnd_3x3[23:16];
  assign x3 = xwnd_3x3[31:24];
  assign x4 = xwnd_3x3[39:32];
  assign x5 = xwnd_3x3[47:40];
  assign x6 = xwnd_3x3[55:48];
  assign x7 = xwnd_3x3[63:56];
  assign x8 = xwnd_3x3[71:64];
  
  assign w0 = w[6:0];
  assign w1 = w[13:7];
  assign w2 = w[20:14];
  assign w3 = w[27:21];
  assign w4 = w[34:28];
  assign w5 = w[41:35];
  assign w6 = w[48:42];
  assign w7 = w[55:49];
  assign w8 = w[62:56];
  
   
  assign psum = x0*w0 + x1*w1 + x2*w2 + x3*w3 + x4*w4 + x5*w5 + x6*w6 + x7*w7 + x8*w8 + adder0;
  
  
  // output select
  wire signed [15:0] slt_output;
  assign slt_output = sp[xcfg_reg[15:12]];
  
  // relu
  wire relu;
  wire signed [15:0] relu_output;
  assign relu = xcfg_reg[16];
  assign relu_output = relu? ((slt_output>0)? slt_output : 0) : slt_output;
  
  // shift
  assign xsout = relu_output >>> (xcfg_reg[20:17]);
  
  endmodule
